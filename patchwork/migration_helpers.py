from itertools import groupby


def deduplicate_persons(person):
    persons = person.objects.all()
    persons = sorted(persons, key=lambda x: x.email.lower())
    persons = groupby(persons, key=lambda x: x.email.lower())

    duplicates = []

    for email, objs in persons:
        objs = list(objs)
        if len(objs) > 1:
            duplicates.append(objs)

    for dupes in duplicates:
        main = dupes.pop()

        for dupe in dupes:
            for comment in dupe.comment_set.all():
                comment.submitter = main
                comment.save()

            for patch in dupe.patch_set.all():
                patch.submitter = main
                patch.save()

            for series in dupe.series_set.all():
                series.submitter = main
                series.save()

            dupe = person.objects.get(pk=dupe.pk)
            dupe.delete()
